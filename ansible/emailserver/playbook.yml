---
- hosts: servers
  gather_facts: no
  vars:
    mongo_user: mongo
    mongo_db_name: main_db

  tasks:
    - name: Set vm.overcommit_memory option to 1 (for redis)
      become: yes
      sysctl:
        name: vm.overcommit_memory
        value: '1'
        sysctl_set: yes
      when: not skip_overcommit|default(false)|bool

    - name: Create emailserver folder
      file:
        path: '{{ dest_dir }}'
        state: directory
        mode: u=rwx,g=rx

    - name: Copy emailserver docker-compose files into created folder
      copy:
        src: files/
        dest: '{{ dest_dir }}/'
        mode: preserve

    - name: Generate mongodb password
      shell: tr -d -c "a-zA-Z0-9" < /dev/urandom | head -c 40
      register: mongodb_pass

    - name: Generate redis password
      shell: tr -d -c "a-zA-Z0-9" < /dev/urandom | head -c 40
      register: redis_pass

    - name: Generate access token for wildduck
      shell: tr -d -c "a-zA-Z0-9" < /dev/urandom | head -c 40
      register: access_token

    - name: Generate zonemta-web password
      shell: tr -d -c "a-zA-Z0-9" < /dev/urandom | head -c 40
      register: zmta_web_pass

    - name: Generate zonemta web cookie secret
      shell: tr -d -c "a-zA-Z0-9" < /dev/urandom | head -c 40
      register: zmta_cookie_secret

    - name: Generate .env file
      copy:
        dest: '{{ dest_dir }}/.env'
        mode: '640'
        backup: yes
        content: |
            DOMAIN_NAME={{ dns_name }}
            MONGO_USER={{ mongo_user }}
            MONGO_DB={{ mongo_db_name }}
            MONGO_PASS={{ mongodb_pass.stdout }}
            REDIS_PASS={{ redis_pass.stdout }}
            WILDDUCK_ACCESS_TOKEN={{ access_token.stdout }}
            ZMTA_WEB_PASS={{ zmta_web_pass.stdout }}
            ZMTA_COOKIE_SECRET={{ zmta_cookie_secret.stdout }}
            ALLOW_JOIN={{ allow_join|default(false)|string|lower }}
            PUBLIC_IP={{ public_ip }}
            VPN_IP={{ vpn_ip }}
            NGINX_PORT=80

    - name: Render coredns config to {{ dest_dir }}/Corefile
      template:
        src: files/Corefile.j2
        dest: '{{ dest_dir }}/Corefile'
        mode: '644'

    - name: Create data folder
      file:
        path: '{{ dest_dir }}/data'
        state: directory
        mode: '755'

    - name: Create redis_data folder
      file:
        path: '{{ dest_dir }}/data/redis'
        state: directory
        mode: '777'

    - name: Generate dkim key for {{ dns_name }}
      shell: docker-compose --env-file {{ dest_dir }}/.env -f {{ dest_dir }}/docker-compose.yml run --rm dkim_config

    - name: Install openssl package
      become: yes
      package:
        name: openssl
        state: present
      when: not skip_dh|default(false)|bool

    - name: Generate DHE parameters for {{ dns_name }}
      shell: openssl dhparam -outform PEM -out {{ dest_dir }}/data/dhparam.pem 2048
      when: not skip_dh|default(false)|bool

    - name: Register haraka restart to obtain new certificates in crontab
      cron:
        name: "email-haraka"
        minute: "15"
        hour: "3"
        weekday: "7" #Sunday
        job: 'docker-compose -f {{ dest_dir}}/docker-compose.yml restart haraka 2>&1 | logger -t emailserver'
        state: present

    - name: Register nginx restart to obtain new certificates in crontab
      cron:
        name: "email-nginx"
        minute: "20"
        hour: "3"
        weekday: "7" #Sunday
        job: 'docker-compose -f {{ dest_dir}}/docker-compose.yml restart nginx 2>&1 | logger -t emailserver'
        state: present

    - name: Install restic package
      become: yes
      package:
        name: restic
        state: present
      tags: [ backup ]
      when: backup|default(true)|bool

    - name: Generate backup password
      shell: tr -d -c "a-zA-Z0-9" < /dev/urandom | head -c 40
      register: restic_pass
      tags: [ backup ]
      when: backup|default(true)|bool

    - name: Generate .backup.env file
      copy:
        dest: '{{ dest_dir }}/.backup.env'
        mode: '640'
        backup: yes
        content: |
            RESTIC_PASSWORD={{ restic_pass.stdout }}
            RESTIC_REPOSITORY={{ dest_dir }}/backups
            #if you use S3 backup
            AWS_ACCESS_KEY_ID=Access_Key_Id
            AWS_SECRET_ACCESS_KEY=Secret_Access_Key
      tags: [ backup ]
      when: backup|default(true)|bool

    - name: Register backup script in crontab
      cron:
        name: "email-backup"
        minute: "0"
        hour: "4"
        job: '{{ dest_dir }}/backup.sh 2>&1 > /dev/null | logger -t emailserver'
        state: present
      tags: [ backup ]
      when: backup|default(true)|bool
