## Generate certificates

Generate TLS certificates for example.com, mx.example.com, smtp.example.com, imap.example.com

## Delete user

Go to wildduck container and enter command:

```
wildduck_adm.py delete -u username
```

or

```
wildduck_adm.py delete -id userid
```

After that don't forget to cleanup **threads** collection in MongoDB:

```
db.threads.deleteMany({"user": ObjectId("User_Id_Here")})
```

## Backup Restore instructions

Extract MongoDB dump from backup:

```
restic restore latest --target $PWD --path /production.mongodump
```

Restore MongoDB database from backup:

```
cat production.mongodump | docker-compose exec -T mongodb mongorestore --uri='mongodb://mongo:Your_Pass_Here@mongodb:27017/main_db?authSource=admin' --archive
```

Restore Redis from backup:

```
restore latest --target $PWD/redis_restore --tag redis
```
