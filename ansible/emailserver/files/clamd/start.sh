#!/bin/sh

set -euo pipefail

mkdir -p /data/tmp
chown -R clamav:clamav /data

# Bootstrap the database if clamav is running for the first time
[ -f /data/main.cvd ] || freshclam

# Run the update in background
freshclam -d

# Run clamav
clamd
