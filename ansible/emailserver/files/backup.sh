#!/bin/sh

set -euo pipefail

dir=$(dirname $(readlink -f $0))
source $dir/.backup.env
source $dir/.env

#export env variables
export $(cut -d= -f1 $dir/.backup.env | grep -v '#')
export $(cut -d= -f1 $dir/.env | grep -v '#')

set +e
restic snapshots > /dev/null 2>&1
code=$?
set -e

[ "$code" -ne 0 ] && restic init
restic backup -q --tag env $dir/.env
docker-compose -f $dir/docker-compose.yml exec -T mongodb \
	mongodump -u=$MONGO_USER -p=$MONGO_PASS --archive \
	 | restic backup -q \
		--tag mongo --stdin --stdin-filename production.mongodump
restic backup -q --tag redis $dir/data/redis

#cleanup snapshots
restic forget --tag env --keep-last 3 --prune
restic forget --tag mongo --keep-last 3 --prune
restic forget --tag redis --keep-last 3 --prune
