#!/usr/bin/env python3

import requests
import argparse
import os
import sys
import json
import getpass
import humanfriendly as hf

api_url = os.environ.get("API_URL", "http://wildduck:8080")

def list_users(args):
    params={}
    if args.limit: params.update({'limit': args.limit})
    if args.next_cursor: params.update({'next': args.next_cursor})
    if args.prev_cursor: params.update({'previous': args.prev_cursor})
    if args.query: params.update({'query': args.query})
    r = requests.get(f'{api_url}/users',params=params,
                     headers={'X-Access-Token': args.token})
    if not r.ok:
        sys.exit(f"failed to retrieve users info: {r.text}")
    result = r.json()
    print(f"total results: {result['total']}")
    print(f"next cursor: {result['nextCursor']}")
    print(f"previous cursor: {result['previousCursor']}")
    print("\t".join(["username","allowed_quota","used_quota",
                     "disabled","suspended"]))
    for user in result['results']:
        print("\t".join(map(str, [user['username'],
                                  user['quota']['allowed'],
                                  user['quota']['used'],
                                  user['disabled'],
                                  user['suspended']])))

def read_pass():
    password = getpass.getpass('Password:')
    password2 = getpass.getpass('Repeat Password:')
    if password != password2:
        sys.exit("Passwords don't match!")
    return password

def add_user(args):
    print(f"add user function with {args.username}")
    password = args.password if args.password else read_pass()
    data = {'username': args.username,
            'password': password,
            'name': args.username,
            'address': f'{args.username}@{args.domain_name}'}
    if args.quota: data.update({'quota': hf.parse_size(args.quota, binary=True)})
    r = requests.post(f'{api_url}/users',json=data,
                      headers={'X-Access-Token': args.token})
    print(f"response code: {r.status_code}")
    print(r.json())

def info_user(args):
    print(f"info_user function with {args.username}")
    user_id = resolve_id(args)
    r = requests.get(f'{api_url}/users/{user_id}',
                      headers={'X-Access-Token': args.token})
    print(f"response code: {r.status_code}")
    print(json.dumps(r.json(), indent=4, sort_keys=True))

def setpass_user(args):
    print(f"setpass_user function with {args.username}")
    password = args.password if args.password else read_pass()
    user_id = resolve_id(args)
    r = requests.put(f'{api_url}/users/{user_id}',data={'password': password},
                     headers={'X-Access-Token': args.token})
    print(f"response code: {r.status_code}")
    print(r.json())

def update_user(args):
    print(f"update_user function with {args.username}")
    data = {}
    if args.quota: data.update({'quota': hf.parse_size(args.quota, binary=True)})
    if args.recipients: data.update({'recipients': args.recipients})
    if args.forwards: data.update({'forwards': args.forwards})
    user_id = resolve_id(args)
    r = requests.put(f'{api_url}/users/{user_id}',data=data,
                     headers={'X-Access-Token': args.token})
    print(f"response code: {r.status_code}")
    print(r.json())

def resolve_id(args):
    print(f"resolving user id for {args.username}")
    r = requests.get(f'{api_url}/users/resolve/{args.username}',
                     headers={'X-Access-Token': args.token})
    if not r.ok:
        sys.exit(f"failed to resolve user id for {args.username}: {r.text}")
    user_id = r.json().get('id')
    print(f"user id for {args.username} resolved to {user_id}")
    return user_id

def remove_all_sessions(args, user_id):
    print(f"removing all sessions for {args.username}")
    r = requests.put(f'{api_url}/users/{user_id}/logout',
                     headers={'X-Access-Token': args.token})
    if not r.ok:
        sys.exit(f"failed to remove all sessions for {args.username}: {r.text}")
    print(r.json())
    print(f"successfully removed all sessions for {args.username}")

#can not login
def suspend_user(args):
    print(f"suspend user function with {args.username}")
    user_id = resolve_id(args)
    #suspend user
    args.flag = not args.restore
    r = requests.put(f'{api_url}/users/{user_id}',data={'suspended': args.flag},
                     headers={'X-Access-Token': args.token})
    if not r.ok:
        sys.exit(f"failed to suspend user id for {args.username}: {r.text}")
    print(r.json())
    print(f"successfully suspended user {args.username}")
    #logs out all user sessions in IMAP
    remove_all_sessions(args, user_id)

#can not login, can not receive messages
def disable_user(args):
    print(f"disable user function with {args.username}")
    user_id = resolve_id(args)
    #disable user
    args.flag = not args.restore
    r = requests.put(f'{api_url}/users/{user_id}',data={'suspended': args.flag, 'disabled': args.flag},
                     headers={'X-Access-Token': args.token})
    if not r.ok:
        sys.exit(f"failed to disable user id for {args.username}: {r.text}")
    print(r.json())
    print(f"successfully disabled user {args.username}")
    #logs out all user sessions in IMAP
    remove_all_sessions(args, user_id)

def delete_user(args):
    user_id = args.userid if args.userid else resolve_id(args)
    print(f"delete user function with {user_id}")
    r = requests.delete(f'{api_url}/users/{user_id}',
                        headers={'X-Access-Token': args.token})
    print(f"response code: {r.status_code}")
    print(r.json())

function_map = {'list': list_users,
                'add': add_user,
                'info': info_user,
                'setpass': setpass_user,
                'update': update_user,
                'suspend': suspend_user,
                'disable': disable_user,
                'delete': delete_user}

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='subcommand to run', metavar='subcommand', required=True)
    parser_listusers = subparsers.add_parser('list', help='list all registered users')
    parser_adduser = subparsers.add_parser('add', help='add new user')
    parser_infouser = subparsers.add_parser('info', help='get info about user')
    parser_setpass = subparsers.add_parser('setpass', help='set user password')
    parser_update = subparsers.add_parser('update', help='update user settings')
    parser_suspend = subparsers.add_parser('suspend', help='suspend account (can not login)')
    parser_disable = subparsers.add_parser('disable', help='disable account (can not login, can not receive messages)')
    parser_delete = subparsers.add_parser('delete', help='delete account (deletes user and address entries from DB and schedules a background task to delete messages)')

    parser_listusers.add_argument('-l', '--limit', type=int, help="how many records to return", metavar="number", default=20)
    parser_listusers.add_argument('-n', '--next_cursor', help="Cursor value for next page", metavar="cursor")
    parser_listusers.add_argument('-p', '--prev_cursor', help="Cursor value for previous page", metavar="cursor")
    parser_listusers.add_argument('-q', '--query', help="Partial match of username or default email address", metavar="query")

    parser_adduser.add_argument("username")
    parser_adduser.add_argument('-p', '--password', help="username password", metavar="password")
    parser_adduser.add_argument('-q', '--quota', help="Allowed quota of the user in bytes (kb/mb/gb etc.)", metavar="quota")

    parser_infouser.add_argument("username")

    parser_setpass.add_argument("username")
    parser_setpass.add_argument('-p', '--password', help="username password", metavar="password")

    parser_update.add_argument("username")
    parser_update.add_argument('-q', '--quota', help="Allowed quota of the user in bytes (kb/mb/gb etc.)", metavar="quota")
    parser_update.add_argument('-r', '--recipients', type=int, help="How many messages per 24 hour can be sent", metavar="number")
    parser_update.add_argument('-f', '--forwards', type=int, help="How many messages per 24 hour can be forwarded", metavar="number")

    parser_suspend.add_argument("username")
    parser_suspend.add_argument("--restore", help="flag for subcommand to restore changes", action='store_true')

    parser_disable.add_argument("username")
    parser_disable.add_argument("--restore", help="flag for subcommand to restore changes", action='store_true')

    delete_group = parser_delete.add_mutually_exclusive_group()
    delete_group.add_argument("-u", "--username", metavar="username")
    delete_group.add_argument("-id", "--userid", metavar="userid")
    args = parser.parse_args()

    args.token = os.environ.get('ACCESS_TOKEN')
    if not args.token:
        sys.exit("ACCESS_TOKEN env variable not set!")
    args.domain_name = os.environ.get('DOMAIN_NAME')
    if not args.domain_name:
        sys.exit("DOMAIN_NAME env variable not set!")
    func = function_map[args.subcommand](args)
