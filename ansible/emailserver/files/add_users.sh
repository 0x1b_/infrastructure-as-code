#!/bin/sh

# exit when any command fails
set -eu

if [ $# -lt 1 ]; then
	echo "$(basename $0) [users_file]"
	exit 1
fi
users_file=$1

while read -u 10 nick; do
	newpass=$(tr -d -c 'a-zA-Z0-9$!^@&' < /dev/urandom | head -c 13)
	printf '%s %s\n' $nick $newpass >> "$users_file.pass"
	docker-compose exec -T wildduck wildduck_adm.py add "$nick" -p "$newpass"
	echo "User $nick created"
done 10<$users_file
