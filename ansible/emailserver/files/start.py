#!/usr/bin/env python3

from jinja2 import Environment, FileSystemLoader
import os, sys

source_dir = os.environ.get("TEMPLATE_SOURCE_DIR", "")
dest_dir = os.environ.get("TEMPLATE_DEST_DIR", "")

if not source_dir:
    sys.stderr.write("Template source directory not specified!\n")
    sys.exit(1)

if not dest_dir:
    sys.stderr.write("Template destination directory not specified!\n")
    sys.exit(1)

env = Environment(loader=FileSystemLoader(source_dir))
for subdir, dirs, files in os.walk(source_dir):
    relative_dir =os.path.relpath(subdir, start=source_dir)
    os.makedirs(os.path.join(dest_dir, relative_dir), exist_ok=True)
    for file in files:
        relative_file=os.path.join(relative_dir, file)
        template = env.get_template(relative_file, globals={'NOT_DEFINED': 'NOT_DEFINED_VAR'})
        template.stream(os.environ).dump(os.path.join(dest_dir, relative_file))


os.execv(sys.argv[1], sys.argv[1:])
