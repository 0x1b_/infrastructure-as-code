#!/bin/bash

function delete_rule() {
        handle=$(nft list chain $1 $2 $3 -a | grep "$4" | head -n1 | rev | cut -d' ' -f1 | rev)
        if [ -n "$handle" ]; then
                echo "deleting rule $4 with handle $handle"
                nft delete rule $1 $2 $3 handle "$handle"
        fi
}

delete_rule ip filter FORWARD "jump wg_forward"
delete_rule ip nat POSTROUTING "jump wg_postrouting"
