#!/bin/bash
set -e
if [ $# -lt 2 ]; then
  echo "$0 [interface] [new_peer.conf]"
  exit 1
fi
iface="$1"
conf="$2"
wg show "$iface" > /dev/null
wg addconf "$iface" "$conf"

wgconf="/etc/wireguard/${iface}.conf"
echo -ne '\n' >> "$wgconf"
cat "$conf" >> "$wgconf"
