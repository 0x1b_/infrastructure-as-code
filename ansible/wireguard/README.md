# Wireguard VPN setup

1. Copy `servers.example` to `servers` file and edit it.
2. Apply ansible script:

```
ansible-playbook -i servers playbook.yml
```

3. On server enable firewall vpn forwarding rules by running script:

```
/etc/wireguard/firewall_up.nft
```

4. Start wireguard and coredns on interface (for instance wg0):

```
systemctl start wg-quick@wg0
systemctl start coredns@wg0
```

or you can just start `coredns@wg0` because it requires `wg-quick@wg0` service unit.

