#!/bin/bash

set -euo pipefail

dir=$(dirname $(readlink -f $0))

#export env variables
set -a
source $dir/.backup.env
source $dir/.env
set +a

set +e
restic snapshots > /dev/null 2>&1
code=$?
set -e

[[ $code -ne 0 ]] && restic init
restic backup -q --tag media $dir/data/synapse $dir/.env
docker-compose -f $dir/docker-compose.yml exec -T postgresql \
	pg_dump -U $DB_USER -d $DB_NAME \
	--column-inserts --rows-per-insert=100 | restic backup -q \
		--tag postgres --stdin --stdin-filename production.sql

#cleanup snapshots
restic forget --tag postgres --keep-last 3 --prune
restic forget --tag media --keep-last 3 --prune
