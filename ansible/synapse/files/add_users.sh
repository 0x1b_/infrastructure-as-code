#!/bin/bash

# exit when any command fails
set -eu

if [ $# -lt 1 ]; then
	echo "$(basename $0) [users_file]"
	exit 1
fi
users_file=$1

while read -u 10 nick; do
	newpass=$(tr -d -c 'a-zA-Z0-9$!^@&' < /dev/urandom | head -c 13)
	printf '%s %s\n' $nick $newpass >> "$users_file.pass"
	docker-compose exec -T synapse register_new_matrix_user -c /data/homeserver.yaml -u "$nick" -p "$newpass" --no-admin http://127.0.0.1:8008
	echo "User $nick created"
done 10<$users_file
