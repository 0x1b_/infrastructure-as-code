#!/bin/bash

set -euo pipefail
source ./.env

if [ $# -lt 1 ]; then
 echo "$0 [username]"
 exit 1
fi
username=$1

echo -n 'AccessToken: '
read -s access_token && echo

userid="%40${username}%3A${DOMAIN_NAME}"

docker-compose run --rm curl \
 curl -i -XPOST http://synapse:8008/_synapse/admin/v1/deactivate/$userid \
 -H 'Content-type: application/json' \
 -H "Authorization: Bearer ${access_token}" \
 -d '{ "erase": true }'
