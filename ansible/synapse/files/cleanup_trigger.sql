UPDATE devices SET ip = '0.0.0.0', user_agent = 'Matr1xx', last_seen = null;
UPDATE user_ips SET ip = '0.0.0.0', user_agent = 'Matr1xx';

CREATE OR REPLACE FUNCTION remove_ip_and_user_agent()
  RETURNS trigger AS
$BODY$
begin
    NEW.ip := '0.0.0.0';
    NEW.user_agent := 'Matr1xx';
    return NEW;
end;
$BODY$ language plpgsql;

drop trigger if exists ip_and_user_agent_trigger on devices;
create trigger ip_and_user_agent_trigger before insert or update on
  devices for each row execute function remove_ip_and_user_agent();

drop trigger if exists ip_and_user_agent_trigger on user_ips;
create trigger ip_and_user_agent_trigger before insert or update on
  user_ips for each row execute function remove_ip_and_user_agent();

CREATE OR REPLACE FUNCTION remove_last_seen()
  RETURNS trigger AS
$BODY$
begin
    NEW.last_seen := null;
    return NEW;
end;
$BODY$ language plpgsql;

drop trigger if exists last_seen_trigger on devices;
create trigger last_seen_trigger before insert or update on
  devices for each row execute function remove_last_seen();

UPDATE users SET creation_ts = null;

CREATE OR REPLACE FUNCTION remove_creation_ts()
  RETURNS trigger AS
$BODY$
begin
    NEW.creation_ts := null;
    return NEW;
end;
$BODY$ language plpgsql;

drop trigger if exists creation_ts_trigger on users;
create trigger creation_ts_trigger before insert or update on
  users for each row execute function remove_creation_ts();


delete from user_daily_visits;
delete from presence_stream;
delete from user_stats_historical;
delete from room_stats_historical;
delete from event_search;

CREATE OR REPLACE FUNCTION remove_row()
  RETURNS trigger AS
$BODY$
begin
    return null;
end;
$BODY$ language plpgsql;

drop trigger if exists user_daily_trigger on user_daily_visits;
create trigger user_daily_trigger before insert or update on
  user_daily_visits for each row execute function remove_row();

drop trigger if exists presence_stream_trigger on presence_stream;
create trigger presence_stream_trigger before insert or update on
  presence_stream for each row execute function remove_row();

drop trigger if exists user_stats_trigger on user_stats_historical;
create trigger user_stats_trigger before insert or update on
  user_stats_historical for each row execute function remove_row();

drop trigger if exists room_stats_trigger on room_stats_historical;
create trigger room_stats_trigger before insert or update on
  room_stats_historical for each row execute function remove_row();

drop trigger if exists event_search_trigger on event_search;
create trigger event_search_trigger before insert or update on
  event_search for each row execute function remove_row();

UPDATE pushers SET app_display_name = 'some_app_name', device_display_name = 'some_device_name';

CREATE OR REPLACE FUNCTION replace_pusher()
  RETURNS trigger AS
$BODY$
begin
    NEW.app_display_name := 'some_app_name';
    NEW.device_display_name := 'some_device_name';
    return NEW;
end;
$BODY$ language plpgsql;

drop trigger if exists pushers_trigger on pushers;
create trigger pushers_trigger before insert or update on
  pushers for each row execute function replace_pusher();
