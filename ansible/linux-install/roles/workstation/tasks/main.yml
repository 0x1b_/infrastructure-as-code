  - name: Install sway, swaylock, mpv, etc.
    become: yes
    pacman:
      name:
        - sway
        - swaylock
        - swayidle
        - swaybg
        - waybar
        - otf-font-awesome # for waybar
        - gnu-free-fonts # for FreeSans font
        - torbrowser-launcher
        - gost
        - pipewire
        - pipewire-pulse
        - pavucontrol
        - wireplumber
        - polkit
        - xdg-desktop-portal-wlr # for screen sharing
        - brightnessctl
        - mpv
        - grim
        - slurp
        - swappy
        - imv
        - wl-clipboard
        - gammastep
        - alacritty
        - fzf
        - lf
        - xorg-server-xwayland #only for chromium
        - iw
        - iwd
        - python-wheel
        - python-pywal
        - ffmpeg
        - fontconfig
        - ttf-liberation
      state: latest
      update_cache: yes

  - name: Download MesloLGS fonts for zsh
    become: yes
    get_url:
      url: '{{ item }}'
      dest: /usr/share/fonts/
      mode: '0644'
    loop:
      - https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
      - https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
      - https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
      - https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf

  - block:
    - name: Refresh font cache
      command: fc-cache -f

    - name: Create home directories
      file:
        path: '{{ item }}'
        state: directory
      loop:
        - ~/tmp
        - ~/bin
        - ~/images

    - name: Configure zsh
      import_tasks: zsh.yml
      tags: [ zsh ]

    - name: Create config directories
      file:
        path: ~/.config/{{ item }}
        state: directory
      loop:
        - sway
        - alacritty
        - waybar
        - wal/templates
        - lf
        - gammastep

    - name: Copy sway config
      copy:
        src: sway_config
        dest: ~/.config/sway/config

    - name: Copy alacritty configs
      copy:
        src: alacritty/
        dest: ~/.config/alacritty/

    - name: Copy waybar configs
      copy:
        src: waybar/
        dest: ~/.config/waybar/

    - name: Copy lf config
      copy:
        src: lfrc
        dest: ~/.config/lf/

    - name: Copy vim config
      copy:
        src: vimrc
        dest: ~/.vimrc

    - name: Copy gammastep config
      copy:
        src: gammastep.conf
        dest: ~/.config/gammastep/config.ini

    - name: Copy wal tty-sequences
      copy:
        src: pywal-tty-sequences
        dest: ~/.config/wal/templates/tty-sequences

    - name: Copy background images
      copy:
        src: backgrounds/
        dest: ~/.config/backgrounds/

    - name: Generate wal colors
      shell: wal -i ~/.config/backgrounds --backend wal -qtsn

    - name: Copy lockscreen script and logo
      copy:
        src: '{{ item }}'
        dest: ~/.config/sway/
        mode: preserve
      loop:
        - lock.sh
        - logo.png

    - name: Copy start-torbrowser script
      copy:
        src: start-torbrowser.sh
        dest: ~/bin/start-torbrowser
        mode: u+rwx

    become: yes
    become_user: '{{ username }}'

  - name: Cleanup pacman cache
    shell: rm -rf /var/cache/pacman/pkg/*
    become: yes

