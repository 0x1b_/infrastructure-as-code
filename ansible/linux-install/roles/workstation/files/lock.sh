#!/bin/bash

set -eu

tmpbg=/tmp/screen.jpeg
logo=~/.config/sway/logo.png
# jpeg screenshot (faster than png)
grim -t jpeg -q 50 $tmpbg
# For more info read this awesome documentation: https://ffmpeg.org/ffmpeg.html
ffmpeg -i $tmpbg -i $logo -filter_complex '[0:v] boxblur=lr=10 [blur];[blur][1:v] overlay=(W-w)/2:(H-h)/2 [out]' -map '[out]' -y $tmpbg
swaylock -K -e --indicator-radius 100 -i $tmpbg && rm $tmpbg
