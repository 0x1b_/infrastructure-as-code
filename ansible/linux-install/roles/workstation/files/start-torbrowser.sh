#!/bin/sh

if [ $# -lt 2 ]; then
    echo "$0 [socks_ip] [socks_port]"
    exit 1
fi

TOR_SKIP_LAUNCH=1 TOR_SOCKS_HOST=$1 TOR_SOCKS_PORT=$2 torbrowser-launcher

