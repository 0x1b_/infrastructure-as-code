#!/bin/sh
if [ $# -lt 1 ]; then
	echo "Usage: $(basename $0) [device]"
	exit 1
fi
DEVICE=$1
if [[ -z $DEVICE || -z $LUKS_PASSWORD ]]; then
	echo "Required env variable is empty!"
	exit 2
fi
echo -n $LUKS_PASSWORD | cryptsetup --type luks2 --pbkdf argon2id --iter-time 3027 --cipher aes-xts-plain64 --key-size 512 --use-random --key-file=- --sector-size 4096 --hash sha512 luksFormat $DEVICE
