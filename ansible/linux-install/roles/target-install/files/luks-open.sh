#!/bin/sh
if [ $# -lt 1 ]; then
	echo "Usage: $(basename $0) [device]"
	exit 1
fi
DEVICE=$1
if [[ -z "$DEVICE" || -z "$LUKS_PASSWORD" || -z "$LUKS_NAME" ]]; then
	echo "Required env variable is empty!"
	exit 2
fi
echo -n $LUKS_PASSWORD | cryptsetup open --type luks2 --key-file=- $DEVICE $LUKS_NAME
