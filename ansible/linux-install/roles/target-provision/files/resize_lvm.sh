#!/bin/bash
# exit when any command fails
set -e

function used_space() {
  df --output=pcent $1 | tr -dc '0-9'
}

function total_space_mb() {
  lvs -o lv_size --units m --noheadings $1 | tr -dc '0-9.' | sed 's/\..*//'
}

function check_lvm() {
  mount_point=$1
  used_pcent=$(used_space $mount_point)
  if (( $used_pcent >= 70 )); then
    echo "mount_point: $mount_point"
    lvm_volume=$(findmnt -n -o SOURCE $mount_point)
    echo "lvm_volume: $lvm_volume"
    total_mb=$(total_space_mb $lvm_volume)
    echo "total_mb: $total_mb"
    delta_mb=$(( $total_mb * 20 / 100 ))
    echo "delta_mb: $delta_mb"
    lvresize --resizefs -L+${delta_mb}m $lvm_volume
  fi
}

check_lvm /
check_lvm /home
