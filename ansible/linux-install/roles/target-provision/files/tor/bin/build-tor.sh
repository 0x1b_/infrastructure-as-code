#!/bin/sh

set -eu

basedir=$(dirname $(readlink -f $0))
tordir=$(readlink -f $basedir/..)

if [ $# -lt 1 ]; then
    echo "$0 [version]"
    exit 1
fi
version=$1
args="--build-arg=TOR_VERSION=$version"

podman build $args -f $tordir/Dockerfile -t tor:$version -t tor:latest $tordir
podman system prune -f

