#!/bin/sh

set -eu

basedir=$(dirname $(readlink -f $0))
tordir=$(readlink -f $basedir/..)

case $# in
    1)
       ipaddr=$1
       torrc=
       ;;
    2) ipaddr=$1
       torrc="$2"
       ;;
    *) echo "$0 ip_address [torrc]"
       exit 1
       ;;
esac

args="-p $ipaddr:9050:9050 -p $ipaddr:9053:9053/udp"
if [ "$torrc" ]; then
    args="$args -v $torrc:/etc/torrc:ro"
fi

sudo $tordir/tor-rules.nft
podman run -d --name tor $args tor
