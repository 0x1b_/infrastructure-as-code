def partition_name(drive_name: str, index: int):
    if drive_name[-1].isdigit():
        return f"{drive_name}p{index}"
    return f"{drive_name}{index}"


class FilterModule(object):
    """Ansible custom filters"""

    def filters(self):
        return {"partition_name": partition_name}
