path "cluster/data/consul" {
  capabilities = ["read"]
}

path "cluster/data/consul/node1" {
  capabilities = ["read"]
}

path "consul/creds/consul-agent-node1-role" {
  capabilities = ["read"]
}

path "pki_consul/issue/consul-server-dc1" {
  capabilities = ["update"]
}
