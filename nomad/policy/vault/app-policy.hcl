path "app/*" {
  capabilities = ["read"]
}

path "database/static-creds/app-*" {
  capabilities = ["read"]
}
