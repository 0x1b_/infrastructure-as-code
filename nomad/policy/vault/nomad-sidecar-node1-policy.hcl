path "cluster/data/nomad" {
  capabilities = ["read"]
}

path "cluster/data/nomad/node1" {
  capabilities = ["read"]
}

path "consul/creds/nomad-agent-role" {
  capabilities = ["read"]
}

path "auth/token/create/nomad-server-node1-role" {
  capabilities = ["update"]
}

path "pki_nomad/issue/nomad-server" {
  capabilities = ["update"]
}
