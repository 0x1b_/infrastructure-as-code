namespace "default" {
  capabilities = ["list-jobs", "read-logs"]
}

node {
  policy = "read"
}

agent {
  policy = "read"
}

operator {
  policy = "read"
}
