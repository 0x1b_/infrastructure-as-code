# Nomad cluster guide

Download Nomad and consul-template binaries:

  * https://www.nomadproject.io/downloads/
  * https://github.com/hashicorp/consul-template

unzip and put them in `~/bin` folder. Append `~/bin` to PATH variable.

Consul-template is needed to dynamically rotate secrets in config files.

## Install CoreDNS, Consul, Vault and Nomad systemctl user services

```
systemctl --user link units/coredns@.service
systemctl --user link units/vault@.service
systemctl --user link units/consul@.service
systemctl --user link units/nomad@.service
systemctl --user link units/consul.sidecar@.service
systemctl --user link units/nomad.sidecar@.service
```

Corresponding config files should be located in:
  * ~/.config/cluster.%i/Corefile.tmpl
  * ~/.config/cluster.%i/vault.hcl.tmpl
  * ~/.config/cluster.%i/consul.hcl.tmpl
  * ~/.config/cluster.%i/nomad.hcl.tmpl
  * ~/.config/cluster.%i/consul.sidecar.hcl.tmpl
  * ~/.config/cluster.%i/nomad.sidecar.hcl.tmpl

where %i denotes systemctl unit parameter, .tmpl means "template". I.e. %i = local corresponds to ~/.config/cluster.local/consul.hcl.tmpl file.

For every config file there must be corresponding env file:
  * ~/.config/cluster.%i/coredns.env
  * ~/.config/cluster.%i/vault.env
  * ~/.config/cluster.%i/consul.sidecar.env
  * ~/.config/cluster.%i/nomad.sidecar.env

Examples of configuration files are located in `config` folder.

## For local setup

Create new dummy network interface:

```
ip link add test-eth type dummy
ip addr add 10.1.23.1/24 dev test-eth
```

Copy config and unit files:

```
./install_local.sh
```

Create env files with `LISTEN_ADDR` variable, etc.

~/.config/cluster.local/coredns.env:

```
LISTEN_ADDR=10.1.23.1
```

~/.config/cluster.local/vault.env:

```
LISTEN_ADDR=10.1.23.1
NODE_ID=node1
```

Run services:

```
systemctl --user start vault@local
systemctl --user start coredns@local
```

Initialize Vault with UI on 10.1.23.1:8200 or run command:

```
export VAULT_ADDR=http://10.1.23.1:8200
vault operator init -key-shares=1 -key-threshold=1 -format=json > vault.json
```

Now Vault is initialized but sealed. You can view the status using the following command.

```
vault status
```

Unseal Vault:

```
vault operator unseal
```

Now Vault is started and fully functional.

Copy Vault root token to ~/.config/cluster.local/consul.sidecar.env file:

```
LISTEN_ADDR=10.1.23.1
VAULT_ADDR=http://10.1.23.1:8200
VAULT_TOKEN=Your_Token_Here
```

Now it is necessary to put consul settings in Vault:

```
vault write cubbyhole/node name=node1
vault secrets enable -path=cluster kv-v2

vault kv put cluster/consul acl=false
vault kv put cluster/consul/node1 server_mode=true
```

Start consul cluster

```
systemctl --user start consul.sidecar@local
```

When you want to stop consul node:
  1. Stop consul.sidecar service
  2. Stop consul service


Setup listen address, etc. in ~/.config/cluster.local/nomad.sidecar.env file:

```
LISTEN_ADDR=10.1.23.1
LISTEN_IFACE=test-eth
VAULT_ADDR=http://10.1.23.1:8200
VAULT_TOKEN=Your_Token_Here
```

Copy Vault root token to ~/.config/cluster.local/nomad.env file:

```
VAULT_TOKEN=Your_Token_Here
```

put nomad settings in Vault:

```
vault kv put cluster/nomad acl=false
vault kv put cluster/nomad/node1 server_mode=true
```

Start nomad cluster

```
systemctl --user start nomad.sidecar@local
```

That's it.

## For secure setup

General steps to setup secure nomad cluster:
1. Start Vault with Raft storage
2. Enable Vault audit
3. Create Vault ACL policy and mapped Vault role for Consul sidecar on specific node and issue Vault periodic token for Consul sidecar based on created policy (to retrieve Consul secrets for specific node)
4. Start Consul sidecar
5. ACL bootstrap Consul cluster and setup Anonymous token policy
6. Issue Consul ACL Management token for Vault (to enable consul secret engine)
7. Enable Consul secret engine in Vault using generated Consul Management token (to issue Consul tokens dynamically)
8. Create Consul ACL policy and mapped Vault role for Consul agent on specific node
<!--- Nomad setup -->
9. Create Consul ACL policy for all Nomad agents and mapped Vault role for Nomad agent on specific node (to generate Consul token dynamically)
10. Create Vault ACL policy and mapped Vault role for Nomad agent on specific node (only for servers) to retrieve secrets in nomad jobs
11. Create Vault ACL policy and mapped Vault role for Nomad sidecar on specific node and issue Vault periodic token for Nomad sidecar based on created policy (to fetch Nomad secrets (TLS certs, etc.) for specific node including Consul token and Vault token to retrieve secrets in nomad jobs)
12. Start Nomad sidecar
13. ACL bootstrap Nomad cluster and setup Anonymous token policy
14. Issue Nomad ACL Management token for Vault (to enable nomad secret engine)
15. Enable Nomad secret engine in Vault using generated Nomad Management token (to issue Nomad tokens dynamically)
<!--- Encryption -->
16. Enable gossip encryption
17. Enable mTLS encryption

How can we automatically rotate tokens on nodes? We will use consul-template that monitors Vault secrets, updates config files and restarts necessary services in background.

An important requirement is that each Nomad agent talks to a unique Consul agent. Nomad agents should be configured to talk to Consul agents and not Consul servers.

Start and initialize Vault and CoreDNS as described in previous section.

Enable Vault audit:

```
vault audit enable file file_path=stdout
```

It enables logs on stdout. This is useful when running in a container.

for more info: [Vault audit](https://www.vaultproject.io/docs/audit).

For High-Availability go through the easy interactive Katacoda tutorial in [Vault HA Cluster with Integrated Storage](https://learn.hashicorp.com/vault/operations/raft-storage) guide to setup HA Vault cluster using Raft storage.


Create "cluster" KV secret engine to store all cluster configurations:

```
vault secrets enable -path=cluster kv-v2
vault write cluster/config max_versions=5
```

## Start Consul


Create Vault ACL policy for Consul sidecar on specific node:

```
vault policy write consul-sidecar-node1-policy policy/vault/consul-sidecar-node1-policy.hcl
```

Create Vault ACL token role for Consul sidecar on specific node:

```
vault write auth/token/roles/consul-sidecar-node1-role allowed_policies="consul-sidecar-node1-policy" token_period=3h token_bound_cidrs="10.1.23.1/32"
```

Issue Vault periodic token for Consul sidecar based on created policy (to retrieve Consul secrets for specific node):

```
vault token create -role=consul-sidecar-node1-role -orphan
```

ACL bootstrap Consul cluster:

The first step for bootstrapping the ACL system is to enable ACLs on the Consul servers in the agent configuration.

```
acl {
  enabled        = true
  default_policy = "deny"
  enable_token_persistence = false
}
```

Note: If you are bootstrapping ACLs on an existing datacenter, enable the ACLs on the agents first with `default_policy=allow`. Default policy allow will enable ACLs, but will allow all operations, allowing the datacenter to function normally while you create the tokens and apply them.

By enabling token persistence, tokens will be persisted to disk and reloaded when an agent restarts.

This step is already done, just put initial settings in Vault for Consul agent on specific node:

```
vault kv put cluster/consul acl=true acl_bootstrap=true
vault kv put cluster/consul/node1 server_mode=true

VAULT_TOKEN=Your_Vault_Periodic_Token_For_Consul_Sidecar vault write cubbyhole/node name=node1
```

Put this Vault token to `~/.config/cluster.local/consul.sidecar.env` file:

```
LISTEN_ADDR=10.1.23.1
VAULT_ADDR=http://10.1.23.1:8200
VAULT_TOKEN=Your_Vault_Periodic_Token_For_Consul_Sidecar
```

Configure consul-template to automatically renew Vault token:

Add `RENEW_TOKEN=true` to `~/.config/cluster.local/consul.sidecar.env` file.

Start Consul:

```
systemctl --user start consul.sidecar@local
```

Create the initial bootstrap token:

```
consul acl bootstrap -format=json > consul.json
```

The bootstrap token is a management token with unrestricted privileges. It will be shared with all the servers in the quorum.
By default, Consul assigns the `global-management` policy to the bootstrap token, which has unrestricted privileges. It is important to have one token with unrestricted privileges in case of emergencies.

Since ACLs have been enabled, you will need to use a token to complete any additional operations.

```
export CONSUL_HTTP_ADDR=http://10.1.23.1:8500
export CONSUL_HTTP_TOKEN=Your_Bootstrap_Token_Here
```

Note, the bootstrap token can only be created once, bootstrapping will be disabled after the bootstrap token was created. Once the ACL system is bootstrapped, ACL tokens can be managed through the ACL API.


Adding tokens to agents is a three step process.
 1. Create the agent policy.
 2. Create the token with the newly created policy.
 3. Add the token to the agent.


Create token policy for Consul agent:

```
consul acl policy create -name "consul-agent-node1-policy" -description "Consul Agent Node1 Policy" -rules @policy/consul/consul-agent-node1-policy.hcl
```

It is best practice to create separate node policies and tokens for each node in the datacenter with an exact-match node rule.

Agents need to be configured with an `acl.tokens.agent` with at least "write" privileges to their own node name in order to register their information with the catalog. Consul's DNS interface is also affected by restrictions on node rules. If the Anonymous token or `acl.token.default` used by the agent does not have "read" access to a given node, then the DNS interface will return no records when queried for it.


Finally, configure the anonymous token:

The anonymous token is created during the bootstrap process, `consul acl bootstrap`. It is implicitly used if no token is supplied. This token, by default, does not provide access to any Consul objects.


Builtin Tokens: During cluster bootstrapping when ACLs are enabled both the special `anonymous` and the `master` token will be injected, also `global-management` policy is created with ID `00000000-0000-0000-0000-000000000001`.
  * **Anonymous Token** - The anonymous token is used when a request is made to Consul without specifying a bearer token. The anonymous token's description and policies may be updated but Consul will prevent this token's deletion. When created, it will be assigned `00000000-0000-0000-0000-000000000002` for its Accessor ID and `anonymous` for its Secret ID.
  * **Master Token** - When a master token is present within the Consul configuration, it is created and will be linked with the builtin `Global Management` policy giving it unrestricted privileges. The master token is created with the Secret ID set to the value of the configuration entry.

In this section you will update the existing token with a newly created anonymous policy.

It is common in many environments to allow listing of all nodes, even without a token. The policies associated with the special anonymous token can be updated to configure Consul's behavior when no token is supplied. The anonymous token is managed like any other ACL token, except that `anonymous` is used for the secret ID

```
consul acl policy create -name "anonymous-policy" -description "Consul Anonymous Policy" -rules @policy/consul/anonymous-policy.hcl
```

set newly crteated policy to anonymous token:

```
consul acl token update -id 00000000-0000-0000-0000-000000000002 -policy-name anonymous-policy -description "Anonymous Token - Can List Nodes And Services"
```

The anonymous token is implicitly used if no token is supplied. The anonymous token is also used for DNS lookups since there is no way to pass a token as part of a DNS request.

Set agent-specific default tokens (optional):

An alternative to the anonymous token is the `acl.tokens.default` configuration item. When a request is made to a particular Consul agent and no token is supplied, the `acl.tokens.default` will be used for the token, instead of being left empty which would normally invoke the anonymous token.
This behaves very similarly to the anonymous token, but can be configured differently on each agent, if desired. For example, this allows more fine grained control of what DNS requests a given agent can service or can give the agent read access to some key-value store prefixes by default.

Builtin Policies:
  * **Global Management** - Grants unrestricted privileges to any token that uses it. When created it will be named `global-management` and will be assigned the reserved ID of `00000000-0000-0000-0000-000000000001`. This policy can be renamed but modification of anything else including the rule set and datacenter scoping will be prevented by Consul.

After setting up access control, you will need to implement a token rotation policy. If you are using third-party tool to generate tokens, such as Vault, Consul ACL tokens will adhere to the TTLs set in that third party tool.

for more info: [Bootstrap the Consul ACL System](https://learn.hashicorp.com/consul/day-0/acl-guide), [Secure Consul with ACLs](https://learn.hashicorp.com/consul/security-networking/production-acls), [Manage ACL Policies](https://learn.hashicorp.com/consul/security-networking/managing-acl-policies), [ACL Documentation and Guides](https://www.consul.io/docs/acl) and Katacoda hands-on guide [Secure Consul Agent Communication with ACL](https://katacoda.com/hashicorp/scenarios/consul-security-acl).


Issue Consul management token for Vault:

```
consul acl token create -policy-name global-management -description "Consul token for Vault"
```

Setup Consul secret engine:

```
vault secrets enable consul
```

Configure Vault to connect and authenticate to Consul:

```
vault write consul/config/access address=${LISTEN_ADDR}:8500 token=Your_Consul_Token_For_Vault
```

Configure a Vault role that maps a name in Vault to a Consul ACL policy:

```
vault write consul/roles/consul-agent-node1-role policies=consul-agent-node1-policy ttl=12h max_ttl=72h
```

After the secrets engine is configured and a user/machine has a Vault token with the proper permission, it can generate credentials.
For testing, generate a new credential by reading from the /creds endpoint with the name of the role:

```
vault read consul/creds/consul-agent-node1-role
```

Finally, set `acl_bootstrap` to `false`, Consul.sidecar should fetch new agent Consul token and restart Consul server automatically:

```
vault kv patch cluster/consul acl_bootstrap=false
```

After ACL bootstrapping Consul must be started in the following order:
  * consul service
  * consul.sidecar service


Create Consul admin role:

```
vault write consul/roles/admin-role policies=global-management ttl=5h max_ttl=12h
```

For testing, generate a new credential by reading from the /creds endpoint with the name of the role:

```
vault read consul/creds/admin-role
```


### Nomad setup

Create Consul ACL policy for all Nomad agents:

```
consul acl policy create -name "nomad-agent-policy" -description "Nomad Agent Policy" -rules @policy/consul/nomad-agent-policy.hcl
```

Create Vault role for all Nomad agents (to generate Consul token dynamically):

```
vault write consul/roles/nomad-agent-role policies=nomad-agent-policy ttl=12h max_ttl=72h
```

For testing, generate a new credential by reading from the /creds endpoint with the name of the role:

```
vault read consul/creds/nomad-agent-role
```


Create Vault role for all Nomad cluster:

```
vault write auth/token/roles/nomad-cluster @policy/vault/nomad-cluster-role.json
```

Nomad servers will be allowed to create Vault tokens with any subset of allowed policies in `nomad-cluster` role.

for more info: [Vault Integration](https://www.nomadproject.io/docs/vault-integration).

Put this Vault role name in Nomad cluster config:

```
vault kv put cluster/nomad vault_role=nomad-cluster
```


Create Vault policy for all Nomad agents (only for servers) to retrieve secrets in nomad jobs:

```
vault policy write nomad-server-policy policy/vault/nomad-server-policy.hcl
```

Create Vault role for Nomad server on specific node (to generate Vault token dynamically):

```
vault write auth/token/roles/nomad-server-node1-role allowed_policies="nomad-server-policy,app-policy" token_period=3h token_bound_cidrs="10.1.23.1/32"
```

Issue Vault periodic token for Nomad server:

```
vault token create -role=nomad-server-node1-role -orphan
```

Put this Vault token to `~/.config/cluster.local/nomad.env` file.

Nomad servers will renew the token automatically. Note that the Nomad clients do not need to be provided with a Vault token.


Create Vault policy for Nomad sidecar on specific node:

```
vault policy write nomad-sidecar-node1-policy policy/vault/nomad-sidecar-node1-policy.hcl
```

Create Vault role for Nomad sidecar on specific node:

```
vault write auth/token/roles/nomad-sidecar-node1-role allowed_policies="nomad-sidecar-node1-policy" token_period=3h token_bound_cidrs="10.1.23.1/32"
```

Issue Vault periodic token for Nomad sidecar based on created role (to retrieve Nomad secrets for specific node):

```
vault token create -role=nomad-sidecar-node1-role -orphan
```

Put this Vault token to `~/.config/cluster.local/nomad.sidecar.env` file:

```
LISTEN_ADDR=10.1.23.1
VAULT_ADDR=http://10.1.23.1:8200
VAULT_TOKEN=Your_Vault_Periodic_Token_For_Nomad_Sidecar
```

Put initial settings in Vault for Nomad agent on specific node:

```
vault kv patch cluster/nomad acl=true
vault kv put cluster/nomad/node1 server_mode=true

VAULT_TOKEN=Your_Vault_Periodic_Token_For_Nomad_Sidecar vault write cubbyhole/node name=node1
```

Configure consul-template to automatically renew Vault token:

Add `RENEW_TOKEN=true` to `~/.config/cluster.local/nomad.sidecar.env` file.

Start Nomad:

```
systemctl --user start nomad.sidecar@local
```

Create the initial bootstrap token:

```
export NOMAD_ADDR=http://10.1.23.1:4646
nomad acl bootstrap > nomad.bootstrap
```

Once the `nomad acl bootstrap` command is run, Nomad's default-deny policy will become enabled. You should have an acceptable anonymous policy prepared and ready to submit immediately after bootstrapping. By default, there is no `anonymous` policy set meaning all anonymous requests are denied.

Since ACLs have been enabled, you will need to use a token to complete any additional operations.

```
export NOMAD_TOKEN=Your_Bootstrap_Token_Here
```

For clients making requests without ACL tokens, you may want to grant some basic level of access. This is done by setting rules on the special "anonymous" policy. This policy is applied to any requests made without a token.

Create `anonymous` policy:

```
nomad acl policy apply -description "Nomad Anonymous Policy" anonymous policy/nomad/anonymous-policy.hcl
```

Issue Nomad management token for Vault (to enable nomad secret engine):

```
nomad acl token create -name="Nomad token for Vault" -type=management -global=true
```

To create management tokens or global tokens, see [Nomad ACL Tokens Fundamentals](https://learn.hashicorp.com/nomad/acls/tokens) and [Nomad Architecture](https://www.nomadproject.io/docs/internals/architecture).


Setup Nomad secret engine:

```
vault secrets enable nomad
```

Configure Vault to connect and authenticate to Nomad:

```
vault write nomad/config/access address=${LISTEN_ADDR}:4646 token=Your_Nomad_Token_For_Vault
vault write nomad/config/lease ttl=5h max_ttl=12h
```

Create Nomad admin role:

```
vault write nomad/role/admin-role type=management global=true
```


After the secrets engine is configured and a user/machine has a Vault token with the proper permission, it can generate credentials.
For testing, generate a new credential by reading from the /creds endpoint with the name of the role:

```
vault read nomad/creds/admin-role
```


## Transport encryption

There are two different systems that need to be configured separately to encrypt communication within the Consul datacenter: gossip encryption and TLS. TLS is used to secure the RPC calls between agents. Gossip communication is secured with a symmetric key, since gossip between agents is done over UDP.

Configure Consul gossip encryption. The key must be 32-bytes, Base64 encoded:

```
consul keygen
```

To enable gossip on a new datacenter, you will add the `encrypt` parameter to the agent configuration file. All nodes within the same datacenter must share the same encryption key in order to send and receive datacenter information, including clients and servers.

```
encrypt = "Your_Gossip_Key_Here"
```

To enable this automatically put following setting in Vault:

```
vault kv patch cluster/consul gossip_key=Your_Gossip_Key_Here
```

Nomad gossip encryption

The same encryption key should be used on every server in a region.

```
nomad operator keygen
```

To enable this automatically put following setting in Vault:

```
vault kv patch cluster/nomad gossip_key=Your_Gossip_Key_Here
```

## TLS encryption

Here we will:

Enable TLS encryption, see [Consul Security Model](https://www.consul.io/docs/internals/security.html):

At a minimum, `verify_outgoing` should be enabled to verify server authenticity with each server having a unique TLS certificate. `verify_server_hostname` is also required to prevent a compromised agent restarting as a server and being given access to all secrets. This setting is critical to prevent a compromised client from being restarted as a server and having all cluster state including all ACL tokens and Connect CA root keys replicated to it.

Enable TLS Encryption for Nomad, see [Enable TLS Encryption for Nomad](https://learn.hashicorp.com/nomad/transport-security/enable-tls):

Nomad optionally uses mutual TLS (mTLS) for all HTTP and RPC communication. Nomad's use of mTLS provides the following properties:
  * Prevent unauthorized Nomad access
  * Prevent observing or tampering with Nomad communication
  * Prevent client/server role or region misconfigurations
  * Prevent other services from masquerading as Nomad agents

We will use Vault for certificate management.


### Vault encryption

Enable TLS encryption for communications with Vault.

Setup PKI secret engine:

```
vault secrets enable pki
vault secrets tune -max-lease-ttl=87600h pki
```

Create Vault root certificate:

```
vault write -field=certificate pki/root/generate/internal key_type=ec key_bits=521 common_name="Vault Root CA" ttl=87600h > vault.root.crt
```

This generates a new self-signed CA certificate and private key. Vault will automatically revoke the generated root at the end of its lease period (TTL).

Configure a role for Vault API that maps a name in Vault to a procedure for generating a certificate:

```
vault write pki/roles/vault-api key_type=ec key_bits=521 allowed_domains=vault.api allow_bare_domains=true max_ttl=43800h
```

Generate a new certificate by writing to the /issue endpoint with the name of the role:

```
vault write -format=json pki/issue/vault-api common_name=vault.api ttl=26280h > vault.api.json
```

Create individual files:

```
cat vault.api.json | jq -r '.data.certificate' > vault.api.crt
cat vault.api.json | jq -r '.data.private_key' > vault.api.key
```

Setup Vault listener: To configure the listener to use a CA certificate, concatenate the primary certificate and the CA certificate together. The primary certificate should appear first in the combined file:

```
cat vault.api.crt vault.root.crt > vault.api.fullchain.crt
```

  * Copy Vault root certificate to `~/data/cluster.local/certs/vault.root.crt`
  * Copy Vault API certificate to `~/data/cluster.local/certs/vault.api.fullchain.crt` (Only for Vault node)
  * Copy Vault API key to `~/data/cluster.local/certs/vault.api.key` (Only for Vault node)

Add `VAULT_TLS=true` to `~/.config/cluster.local/vault.env`. Restart Vault service.

Setup env variables for cli:

```
export VAULT_CACERT=~/data/cluster.local/certs/vault.root.crt
export VAULT_ADDR=https://10.1.23.1:8200
export VAULT_TLS_SERVER_NAME=vault.api
```

Add `VAULT_TLS=true` to `~/.config/cluster.local/consul.sidecar.env` and `~/.config/cluster.local/nomad.sidecar.env`. Change http to https and restart sidecars.

for more info: [PKI Secrets Engine](https://www.vaultproject.io/docs/secrets/pki), [Build Your Own Certificate Authority (CA)](https://learn.hashicorp.com/vault/secrets-management/sm-pki-engine) and [Generate mTLS Certificates for Nomad using Vault](https://learn.hashicorp.com/nomad/vault-integration/vault-pki-nomad).

### Consul encryption

Generate the intermediate CA:

```
vault secrets enable -path=pki_consul pki
```

Tune the PKI secrets engine at the `pki_consul` path to issue certificates with a maximum time-to-live (TTL) of 43800 hours.

```
vault secrets tune -max-lease-ttl=43800h pki_consul
```

Generate a CSR from your intermediate CA and save it as `pki_consul.csr`.

```
vault write -format=json pki_consul/intermediate/generate/internal \
    key_type=ec key_bits=521 common_name="global.consul Intermediate Authority" \
    ttl="43800h" | jq -r '.data.csr' > pki_consul.csr
```

Sign the intermediate CA CSR with the root certificate and save the generated certificate as `consul.ca.crt`.

```
vault write -format=json pki/root/sign-intermediate \
    csr=@pki_consul.csr format=pem_bundle \
    ttl="43800h" | jq -r '.data.certificate' > consul.ca.crt
```

Once the CSR is signed and the root CA returns a certificate, it can be imported back into Vault.

```
vault write pki_consul/intermediate/set-signed certificate=@consul.ca.crt
```

Create a role named `consul-server-dc1` that specifies the allowed domains and enables to generate certificates with a TTL of 72 hours.

```
vault write pki_consul/roles/consul-server-dc1 \
    allowed_domains=server.dc1.hackspace allow_bare_domains=true \
    key_type=ec key_bits=521 ttl=72h max_ttl=72h require_cn=false
```

Create a role named `consul-client-dc1` that specifies the allowed domains and enables to generate certificates with a TTL of 72 hours.

```
vault write pki_consul/roles/consul-client-dc1 \
    allowed_domains=client.dc1.hackspace allow_bare_domains=true \
    key_type=ec key_bits=521 ttl=72h max_ttl=72h require_cn=false
```

For testing, issue certificate:

```
vault write -format json pki_consul/issue/consul-server-dc1 common_name=server.dc1.hackspace
```

Append rule to consul-sidecar-node1-policy.hcl file

```
path "pki_consul/issue/consul-server-dc1" {
  capabilities = ["update"]
}
```

Update consul-sidecar-node1-policy policy:

```
vault policy write consul-sidecar-node1-policy policy/vault/consul-sidecar-node1-policy.hcl
```

Add `CONSUL_TLS=true` to `~/.config/cluster.local/consul.sidecar.env`.

### Nomad encryption

Generate the intermediate CA:

```
vault secrets enable -path=pki_nomad pki
```

Tune the PKI secrets engine at the `pki_nomad` path to issue certificates with a maximum time-to-live (TTL) of 43800 hours.

```
vault secrets tune -max-lease-ttl=43800h pki_nomad
```

Generate a CSR from your intermediate CA and save it as `pki_nomad.csr`.

```
vault write -format=json pki_nomad/intermediate/generate/internal \
    key_type=ec key_bits=521 common_name="global.nomad Intermediate Authority" \
    ttl="43800h" | jq -r '.data.csr' > pki_nomad.csr
```

Sign the intermediate CA CSR with the root certificate and save the generated certificate as `nomad.ca.crt`.

```
vault write -format=json pki/root/sign-intermediate \
    csr=@pki_nomad.csr format=pem_bundle \
    ttl="43800h" | jq -r '.data.certificate' > nomad.ca.crt
```

Once the CSR is signed and the root CA returns a certificate, it can be imported back into Vault.

```
vault write pki_nomad/intermediate/set-signed certificate=@nomad.ca.crt
```

Create a role named `nomad-server` that specifies the allowed domains and enables to generate certificates with a TTL of 72 hours.

```
vault write pki_nomad/roles/nomad-server \
    allowed_domains=server.global.nomad allow_bare_domains=true \
    key_type=ec key_bits=521 ttl=72h max_ttl=72h require_cn=false
```

Create a role named `nomad-client` that specifies the allowed domains and enables to generate certificates with a TTL of 72 hours.

```
vault write pki_nomad/roles/nomad-client \
    allowed_domains=client.global.nomad allow_bare_domains=true \
    key_type=ec key_bits=521 ttl=72h max_ttl=72h require_cn=false
```

For testing, issue certificate:

```
vault write -format json pki_nomad/issue/nomad-server common_name=server.global.nomad
```

Append rule to nomad-sidecar-node1-policy.hcl file

```
path "pki_nomad/issue/nomad-server" {
  capabilities = ["update"]
}
```

Update nomad-sidecar-node1-policy policy:

```
vault policy write nomad-sidecar-node1-policy policy/vault/nomad-sidecar-node1-policy.hcl
```

Add `NOMAD_TLS=true` to `~/.config/cluster.local/nomad.sidecar.env`.
