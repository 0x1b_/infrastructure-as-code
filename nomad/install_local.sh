#!/bin/sh

set -euo pipefail
script_dir=$(dirname $(readlink -f $0))
systemctl --user --force link $script_dir/units/coredns@.service
systemctl --user --force link $script_dir/units/vault@.service
systemctl --user --force link $script_dir/units/consul@.service
systemctl --user --force link $script_dir/units/nomad@.service
systemctl --user --force link $script_dir/units/consul.sidecar@.service
systemctl --user --force link $script_dir/units/nomad.sidecar@.service

mkdir -p ~/.config/cluster.local
cp -rf $script_dir/config/* ~/.config/cluster.local
