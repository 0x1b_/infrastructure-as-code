#!/bin/sh 

if [ $# -lt 1 ]; then
    echo "$0 [serviceID]"
    exit 1
fi

curl -X PUT -H "X-Consul-Token: ${CONSUL_HTTP_TOKEN}" -H "Content-Type: application/json" -d "{\"Node\":\"node1\", \"ServiceID\": \"$1\"}" ${CONSUL_HTTP_ADDR}/v1/catalog/deregister
