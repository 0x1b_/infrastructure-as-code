# vm setup

To build gateway, from this directory run:

```
packer build -var user_pass=$(uuidgen) -var playbook=path-to-ansible/linux-install/playbook.yml  -only="gateway.*" -on-error=ask arch-template.pkr.hcl
```

You can also specify tor version.

To build workstation, from this directory run:

```
packer build -var user_pass=$(uuidgen) -var playbook=path-to-ansible/linux-install/playbook.yml  -only="workstation.*" -on-error=ask arch-template.pkr.hcl
```

# VirtualBox tips

To start sway successfully, enable option: Display -> Enable 3D Acceleration.

# Running VM inside Sway

change in `~/.config/sway/config`:
  * Mod4 to Mod1
  * lalt_lshift_toggle to lctrl_lalt_toggle
