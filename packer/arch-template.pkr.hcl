variable "headless" {
    type = bool
    default = false
    description = "Run VMs in headless mode during build"
}

variable "user_pass" {
    type = string
    description = "SSH password"
}

variable "playbook" {
    type = string
    description = "ArchLinux Playbook filepath"
}

variable "tor_version" {
    type = string
    default = "0.4.6.7"
    description = "Tor version to install"
}

variable "output_format" {
    type = string
    default = "ova"
    description = "VirtualBox output format"
}

source "virtualbox-iso" "arch-base" {
    iso_url = "https://mirrors.kernel.org/archlinux/iso/{{isotime `2006.01`}}.01/archlinux-{{isotime `2006.01`}}.01-x86_64.iso"
    iso_checksum = "file:https://mirrors.kernel.org/archlinux/iso/{{isotime `2006.01`}}.01/md5sums.txt"
    iso_interface = "sata"

    guest_os_type = "ArchLinux_64"
    # size in mb
    disk_size = 15360
    hard_drive_interface = "sata"
    # SSD
    hard_drive_nonrotational = true 
    hard_drive_discard = true
    sata_port_count = 5
    # disable uploading VirtualBox version file
    virtualbox_version_file = "" 
    headless = var.headless
    format = var.output_format
    guest_additions_mode = "disable"

    # builder options
    cpus = 2
    memory = 1500

    # communicator options
    ssh_username = "root"
    ssh_password = var.user_pass

    http_directory = "${path.root}/files"
    http_bind_address = "127.0.0.1"
    boot_wait = "5s"
    boot_command = [
        "<enter><wait120s>",
        "/usr/bin/curl -O http://{{ .HTTPIP }}:{{ .HTTPPort }}/enable-ssh.sh<enter><wait5>",
        "USER_PASS=${var.user_pass} /usr/bin/bash ./enable-ssh.sh<enter><wait1>",
    ]
    shutdown_command = "systemctl poweroff"

}

build {
    # use the `name` field to name a build in the logs.
    name = "gateway"

    source "virtualbox-iso.arch-base" {
        name = "gateway" # for logs
        vm_name = "gateway"
        output_directory = "output/gateway"
        export_opts = [ "--vsys", "0", "--description", "Gateway for workstation" ]

        vboxmanage = [
            ["modifyvm", "{{.Name}}", "--firmware", "EFI"],
            ["modifyvm", "{{.Name}}", "--memory", "1000"],
            ["modifyvm", "{{.Name}}", "--cpus", "2"],
            ["modifyvm", "{{.Name}}", "--graphicscontroller", "vmsvga"],
            ["modifyvm", "{{.Name}}", "--rtcuseutc", "on"]
        ]
    }

    provisioner "ansible" {
      playbook_file = var.playbook
      user = "root"
      inventory_file_template = "gateway ansible_host={{.Host}} ansible_user={{.User}} ansible_port={{.Port}} ansible_remote_tmp=/tmp main_drive=/dev/sda home_size=2G root_size=7G swap=1G encrypt=false username=osiris bootloader_name=systemd-boot sshd=false wait-online=false logging=false firmware=false optpkgs=false ntp=false gateway=true vm=true tor_version=${var.tor_version}"
    }
}

build {
    # use the `name` field to name a build in the logs.
    name = "workstation"

    source "virtualbox-iso.arch-base" {
        name = "workstation" # for logs
        vm_name = "workstation"
        output_directory = "output/workstation"
        export_opts = [ "--vsys", "0", "--description", "Workstation" ]

        vboxmanage = [
            ["modifyvm", "{{.Name}}", "--firmware", "EFI"],
            ["modifyvm", "{{.Name}}", "--memory", "4000"],
            ["modifyvm", "{{.Name}}", "--cpus", "2"],
            ["modifyvm", "{{.Name}}", "--graphicscontroller", "vboxsvga"],
            ["modifyvm", "{{.Name}}", "--vram", "256"],
            ["modifyvm", "{{.Name}}", "--rtcuseutc", "on"]
        ]
    }

    provisioner "ansible" {
      playbook_file = var.playbook
      user = "root"
      inventory_file_template = "workstation ansible_host={{.Host}} ansible_user={{.User}} ansible_port={{.Port}} ansible_remote_tmp=/tmp main_drive=/dev/sda home_size=2G root_size=7G swap=1G encrypt=false username=osiris bootloader_name=systemd-boot sshd=false wait-online=false logging=false firmware=false ipforward=false ntp=false workstation=true vm=true"
    }
}
