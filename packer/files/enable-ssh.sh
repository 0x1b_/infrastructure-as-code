#!/bin/sh

set -e

if [ -z "${USER_PASS}" ]; then
    echo "USER_PASS env empty!"
    exit 1
fi

echo "root:${USER_PASS}" | chpasswd
systemctl start sshd
