#!/usr/bin/env python

from passlib.hash import sha512_crypt
import getpass 
import sys

p1 = getpass.getpass()
p2 = getpass.getpass()
if p1 != p2:
    print("Passwords don't match")
    sys.exit(2)
print(sha512_crypt.using(rounds=77777).hash(p1))
