#!/bin/sh
set -e -o pipefail
set -x

PROGRAM="${0##*/}"

cmd_usage() {
	cat >&2 <<-_EOF
	Usage: $PROGRAM [ up | down | build | ps | add | del ] [ CONFIG_FILE | IP_ADDRESS ]

	  CONFIG_FILE is a configuration file, whose filename is the interface name
	  followed by \`.conf'. IP_ADDRESS in local LAN to add/del from routing through VPN
	_EOF
}

cmd_up() {
   if [[ $# -lt 2 ]]; then
    cmd_usage
    exit 1
  fi
  export WG_FILE=$2
  docker-compose up -d
  sysctl -w net.ipv4.ip_forward=1
  ip route add default via 172.18.1.2 dev awg_bridge table 2345
  echo "vpn started"
}

cmd_down() {
   if [[ $# -lt 2 ]]; then
    cmd_usage
    exit 1
  fi
  export WG_FILE=$2
  docker-compose down
  ip route del default via 172.18.1.2 dev awg_bridge table 2345 || true
  echo "vpn stopped"
}

cmd_ps() {
   if [[ $# -lt 2 ]]; then
    cmd_usage
    exit 1
  fi
  export WG_FILE=$2
  docker-compose ps
}

cmd_build() {
   if [[ $# -lt 2 ]]; then
    cmd_usage
    exit 1
  fi
  export WG_FILE=$2
  docker-compose build
}

cmd_addip() {
  if [[ $# -lt 2 ]]; then
    cmd_usage
    exit 1
  fi
  ip rule add from $2 lookup 2345
}

cmd_delip() {
  if [[ $# -lt 2 ]]; then
    cmd_usage
    exit 1
  fi
  ip rule del from $2 lookup 2345
}


if [[ $# -lt 1 ]]; then
  cmd_usage
  exit 0
fi

case "$1" in
up) cmd_up $@ ;;
down) cmd_down $@ ;;
build) cmd_build $@ ;;
ps) cmd_ps $@ ;;
add) cmd_addip $@ ;;
del) cmd_delip $@ ;;
esac

