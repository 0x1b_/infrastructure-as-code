#!/bin/bash

# turn on bash's job control
set -m
# exit on first error
set -e -o pipefail

amneziawg-go -f wg0 &

echo "Initialize wg0 interface"
sleep 2

awg-quick up wg0
#awg setconf wg0 /wg0.conf
#ip link set dev wg0 up
#ip addr add 10.1.1.1/32 dev wg0
#ip route add default dev wg0 table 2345
#ip rule add not fwmark 1234 table 2345
#ip rule add table main suppress_prefixlength 0
ip rule add iif wg0 lookup main
nft -f /etc/nftables.d/wireguard.nft

fg %1
