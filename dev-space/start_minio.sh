#!/bin/sh
mkdir -p minio_data
docker run --rm -p 127.0.0.1:9000:9000 \
	 --user $(id -u):$(id -g) \
	 -e "MINIO_ACCESS_KEY=ABCDEF" \
	 -e "MINIO_SECRET_KEY=DEFGHIJK" \
	 -v $PWD/minio_data:/data \
	   minio/minio server /data
