#!/bin/sh

set -veuo pipefail

scriptdir=$(dirname $(readlink -f $0))
customdir=/data/gitea
workdir=/data/workdir

mkdir -p $customdir/conf $workdir
cp -rf $scriptdir/app.ini $customdir/conf
cp -rf $scriptdir/public $customdir

gitea embedded -c /dev/null extract --overwrite --dest-dir $customdir 'templates/base/head*'
cp -rf $scriptdir/templates/. $customdir/templates
sed -i 's|favicon.png|favicon.ico|g' $customdir/templates/base/head.tmpl
sed -i 's|gitea-sm.png|weapons.svg|g' $customdir/templates/base/head_navbar.tmpl
chown -R git:git $customdir $workdir

cat <<HELLO >> $customdir/conf/app.ini
[database]
DB_TYPE = ${DB_TYPE}
HOST    = ${DB_HOST}
NAME    = ${DB_NAME}
USER    = ${DB_USER}
PASSWD  = ${DB_PASSWD}
LOG_SQL = false
[server]
ROOT_URL = ${ROOT_URL}
SSH_DOMAIN = ${SSH_DOMAIN}
HELLO

export GITEA_WORK_DIR=$workdir
exec /usr/bin/entrypoint $@
