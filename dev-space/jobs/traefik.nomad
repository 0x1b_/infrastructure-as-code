job "traefik" {
    datacenters = ["dc1"]
    type = "system"

    group "traefik" {
        count = 1
        task "traefik" {
            driver = "docker"
            config {
                image = "traefik:2.2"
                dns_servers = ["${meta.dns}"]
                port_map {
                    http = 80
                    ssh = 22
                    postgres = 5432
                }
                volumes = [
                    "local/traefik.toml:/etc/traefik/traefik.toml",
                    "local/config.toml:/etc/traefik/config.toml"
                ]
            }
            resources {
                cpu = 100 #MHz
                memory = 100 #MB
                network {
                    mode = "bridge"
                    port "http" {
                        static = 80
                    }
                    port "ssh" {
                        static = 22
                    }
                    port "postgres" {
                        static = 5432
                    }
                }
            }
            logs {
                max_files = 2
                max_file_size = 5
            }
            template {
                data = <<END
                # Dynamic Configuration for traefik dashboard
                [http.routers.dashboard]
                rule = "Host(`traefik.service.{{ env "meta.domain" }}`)"
                service = "api@internal"
                
                [http.routers.ping]
                rule = "Host(`traefik.service.{{ env "meta.domain" }}`) && Path(`/ping`)"
                service = "ping@internal"
                END
                destination = "local/config.toml"
                perms = "400"
            }
            template {
                data = <<END
                [entryPoints.http]
                address = ":80"

                [entryPoints.ssh]
                address = ":22"

                [entryPoints.postgres]
                address = ":5432"

                [ping]
                manualRouting = true

                [api]
                dashboard = true

                [providers.file]
                filename = "/etc/traefik/config.toml"

                # Enable Consul Catalog configuration backend.
                [providers.consulCatalog]
                prefix = "traefik"
                exposedByDefault = false
                defaultRule = "Host(`{{ .Name }}.service.{{{ env "meta.domain" }}}`)"
                [providers.consulCatalog.endpoint]
                address = "{{{ env "meta.dns" }}}:8500"
                scheme = "http"
                END
                left_delimiter = "{{{"
                right_delimiter = "}}}"
                destination = "local/traefik.toml"
                perms = "400"
            }
            # integration with Consul for service discovery/monitoring
			service {
				name = "traefik"
				tags = ["http"]
				port = "http"
        		check {
          			type     = "http"
          			path     = "/ping"
          			interval = "15s"
          			timeout  = "20s"
                    header {
                        Host = ["traefik.service.${meta.domain}"]
                    }
                    check_restart {
                        limit = 3
                        grace = "30s"
                    }
        		}
			}
        }
        # restart on local node
        # check -> check_restart -> restart
        restart {
            attempts = 4
            delay = "15s"
            interval = "1m"
            mode = "fail"
        }
    }
}
