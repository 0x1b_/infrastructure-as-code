job "drone" {
	datacenters = ["dc1"]
	group "drone-group" {
		count = 1
    	volume "drone-data" {
      	    type = "host"
      		source = "drone-data"
    	}
		task "drone-task" {
			driver = "docker"
			config {
        		image = "drone/drone:1"
				force_pull = true
				dns_servers = ["${meta.dns}"]
				port_map {
					http = 80
				}
      		}
			resources {
				network {
					mode = "bridge"
					port "http" {}
				}
				cpu 	= 100 #MHz
				memory	= 100 #MB
			}
      		volume_mount {
        		volume = "drone-data"
        		destination = "/data"
      		}
            logs {
                max_files = 2
                max_file_size = 5
            }
			env {
				DRONE_GITEA_SERVER = "http://git.service.${meta.domain}"
                DRONE_GITEA_CLIENT_ID = "9d00515a-52d3-4d4f-bb0f-87aefda4e7b0"
                DRONE_GITEA_CLIENT_SECRET = "LCLGrKY5EfN9w6Ulz7st1xUBVYF4lFd1LD8QsUmPdy0="
				DRONE_GIT_ALWAYS_AUTH = false
				DRONE_SERVER_HOST =  "drone.service.${meta.domain}"
				DRONE_SERVER_PROTO =  "http"
				DRONE_TLS_AUTOCERT = false
                DRONE_USER_CREATE = "username:mind,admin:true"
				#disable all suspicious plugins
				DRONE_JSONNET_ENABLED = false
			}
            template {
                data = <<END
                {{ with secret "app/drone" }}
                DRONE_GITEA_CLIENT_ID = "{{.Data.client_id}}"
                DRONE_GITEA_CLIENT_SECRET = "{{.Data.client_secret}}"
                {{ end }}
                {{ with secret "app/drone-runner" }}
                DRONE_RPC_SECRET = "{{.Data.rpc_secret}}"
                {{ end }}
                END
                destination = "secrets/creds"
                perms = "000"
                env = true
            }
            # integration with Consul for service discovery/monitoring
			service {
				name = "drone"
				tags = [
                    "http",
                    "traefik.enable=true",
                    "traefik.http.routers.drone.entrypoints=http"
                ]
				port = "http"
        		check {
          			type     = "http"
          			path     = "/"
          			interval = "60s"
          			timeout  = "20s"
        		}
			}
		}
	}
}
