job "gitea" {
    datacenters = ["dc1"]
    group "gitea" {
        count = 1
        volume "gitea-data" {
            type = "host"
            source = "gitea-data"
        }
        task "gitea" {
            driver = "docker"
            config {
                image = "hackspace/gitea:1.12.5"
                dns_servers = ["${meta.dns}"]
                port_map {
                    http = 3000
                    ssh = 22
                }
            }
            resources {
                cpu = 300 #MHz
                memory = 1000 #MB
                network {
                    mode = "bridge"
                    port "http" {}
                    port "ssh" {}
                }
            }
            volume_mount {
                volume = "gitea-data"
                destination = "/data"
            }
            logs {
                max_files = 2
                max_file_size = 5
            }
            env {
                USER_UID = 1000
                USER_GID = 1000
                DB_TYPE = "postgres"
                DB_HOST = "postgres.service.${meta.domain}:5432"
                DB_NAME = "gitea"
                ROOT_URL = "http://git.service.${meta.domain}/"
                SSH_DOMAIN = "git.service.${meta.domain}"
            }
            template {
                data = <<END
                {{ with secret "database/static-creds/app-gitea" }}
                DB_USER={{.Data.username}}
                DB_PASSWD={{.Data.password}}
                {{ end }}
                END
                destination = "secrets/creds"
                perms = "000"
                env = true
            }
            # integration with Consul for service discovery/monitoring
			service {
				name = "git"
				tags = [
                    "http",
                    "traefik.enable=true",
                    "traefik.http.routers.git.entrypoints=http"
                ]
				port = "http"
        		check {
          			type     = "http"
          			path     = "/"
          			interval = "15s"
          			timeout  = "20s"
                    check_restart {
                        limit = 3
                        grace = "60s"
                    }
        		}
			}
            service {
                name = "git"
				tags = [
                    "ssh",
                    "traefik.enable=true",
                    "traefik.tcp.routers.git.entrypoints=ssh",
                    "traefik.tcp.routers.git.rule=HostSNI(`*`)"
                ]
                port = "ssh"
                check {
                    type = "tcp"
                    interval = "15s"
                    timeout = "20s"
                    check_restart {
                        limit = 3
                        grace = "60s"
                    }
                }
            }
        }
        # restart on local node
        restart {
            attempts = 4
            delay = "15s"
            interval = "1m"
            mode = "fail"
        }
        # check -> check_restart -> restart -> reschedule
        reschedule {
            delay = "1m"
            delay_function = "constant"
            unlimited = true
        }
    }
}

