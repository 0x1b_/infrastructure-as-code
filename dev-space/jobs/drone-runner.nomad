job "drone-runner" {
	datacenters = ["dc1"]
	group "drone-runner-group" {
		count = 1
        volume "docker-sock" {
            type = "host"
            source = "docker-sock"
        }
		task "drone-runner-task" {
			driver = "docker"
			config {
        		image = "drone/drone-runner-docker:1"
				force_pull = true
				dns_servers = ["${meta.dns}"]
      		}
			resources {
				network {
					mode = "bridge"
				}
				cpu 	= 100 #MHz
				memory	= 100 #MB
			}
            volume_mount {
                volume = "docker-sock"
                destination = "/var/run/docker.sock"
            }
			env {
				DRONE_RPC_HOST =  "drone.service.${meta.domain}"
				DRONE_RPC_PROTO =  "http"
                DRONE_RUNNER_CAPACITY = 2
			}
            template {
                data = <<END
                {{ with secret "app/drone-runner" }}
                DRONE_RPC_SECRET = "{{.Data.rpc_secret}}"
                {{ end }}
                END
                destination = "secrets/creds"
                perms = "000"
                env = true
            }
		}
	}
}

