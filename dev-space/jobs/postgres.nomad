
job "postgres" {
    datacenters = ["dc1"]
    group "postgres" {
        count = 1
        volume "postgres-data" {
            type = "host"
            source = "postgres-data"
        }
        task "postgres" {
            driver = "docker"
            config {
                image = "postgres:12-alpine"
                dns_servers = ["${meta.dns}"]
                port_map {
                    tcp = 5432
                }
            }
            resources {
                cpu = 300 #MHz
                memory = 400 #MB
                network {
                    mode = "bridge"
                    port "tcp" {}
                }
            }
            volume_mount {
                volume = "postgres-data"
                destination = "/var/lib/postgresql/data"
            }
            logs {
                max_files = 2
                max_file_size = 5
            }
            env {
                POSTGRES_INITDB_ARGS = "--lc-collate=en_US.UTF-8 --lc-ctype=en_US.UTF-8 --auth-host=scram-sha-256"
                POSTGRES_HOST_AUTH_METHOD = "scram-sha-256"
            }
            template {
                data = <<END
                {{ with secret "app/postgres" }}
                POSTGRES_DB={{.Data.dbname}}
                POSTGRES_USER={{.Data.user}}
                POSTGRES_PASSWORD={{.Data.pass}}
                {{ end }}
                END
                destination = "secrets/creds"
                perms = "000"
                env = true
            }
            # integration with Consul for service discovery/monitoring
            service {
                name = "postgres"
                tags = [
                    "traefik.enable=true",
                    "traefik.tcp.routers.postgresrouter.entrypoints=postgres",
                    "traefik.tcp.routers.postgresrouter.rule=HostSNI(`*`)"
                ]
                port = "tcp"
                check {
                    type = "tcp"
                    interval = "15s"
                    timeout = "30s"
                    check_restart {
                        limit = 3
                        grace = "60s"
                    }
                }
            }
        }
        # restart on local node
        restart {
            attempts = 4
            delay = "15s"
            interval = "1m"
            mode = "fail"
        }
        # check -> check_restart -> restart -> reschedule
        reschedule {
            delay = "1m"
            delay_function = "constant"
            unlimited = true
        }
    }
}
