## Build gitea image

Inside `gitea` folder build docker image:

```
docker build -t hackspace/gitea:1.12.1 .
```

## Setup traefik service

Submit traefik system job to proxy traffic:

```
nomad run jobs/traefik.nomad
```

## Setup postgresql service

To setup postgresql service we need to initialize database and Vault database secret engine.

Create password for root admin database user. You can manually create random password strings using commands like:

```
cat /dev/urandom | tr -d -c 'a-zA-Z0-9' | head -c 27
```

or enable Transit secret engine:

```
vault secrets enable transit
```

and generate high-entropy random bytes of the specified length using Vault:

```
vault write transit/random format=hex urlbytes=27
```

for more info you can run:

```
vault path-help transit
```


Put database user name and generated password to Vault:

enable kv secret engine on "app/" path

```
vault secrets enable -path=app kv
```

put db admin username, pass and initial db name into it:

```
vault write app/postgres user=root pass=Your_Root_Pass_Here dbname=postgres
```

Append following rule to vault `app-policy` to allow Nomad retrieve app secrets:

```
path "app/*" {
  capabilities = ["read"]
}
```

Update `app-policy`:

```
vault policy write app-policy policy/vault/app-policy.hcl
```

Submit postgres job to nomad:

```
nomad run jobs/postgres.nomad
```

It will fetch db secrets from Vault and create specified database and database user with provided password.
In this case, postgresql job will create "root" admin user and initial database "postgres". This database will be used
by us only for maintenance purposes (to perform migrations).


## DB migrations

Before submitting any service that consume postgresql databases, we should set up postgresql cluster.
A database cluster is a collection of databases that are managed by a single server instance. Every application
will connect to its own database. 

Build flyway docker image (to overcome this bug https://github.com/flyway/flyway/issues/2594 ):

```
docker build -f flyway/Dockerfile -t hackspace/flyway ./flyway
```

Create `db.env` file with the following content:

```
FLYWAY_URL=jdbc:postgresql://postgres.service.hackspace:5432/postgres
FLYWAY_USER=root
FLYWAY_PASSWORD=Your_Pass_Here
```

Run DB migrations to create necessary databases in cluster:

```
docker run --net=host --rm --env-file ./db.env -v $PWD/flyway/db-migrations:/flyway/sql:ro  hackspace/flyway migrate
```

Flyway will create `flyway_schema_history` table in public schema of "postgres" database (for keeping records about already applied migrations)
and finally perform all migrations.

## Dynamic Database Credentials

Create admin db user for Vault:

```
CREATE USER vault WITH SUPERUSER PASSWORD 'Initial_Pass_Here';
```

Enable database secrets engine:

```
vault secrets enable -path database/ database
```

Configure PostgreSQL secrets engine with Vault admin db user:

```
vault write database/config/postgresql \
        plugin_name=postgresql-database-plugin \
        allowed_roles="*" \
        connection_url='postgresql://{{username}}:{{password}}@postgres.service.hackspace:5432/postgres?sslmode=disable' \
        username="vault" \
        password="Initial_Pass_Here"
```

Rotate Vault admin db password:

```
vault write -force database/rotate-root/postgresql
```

`-force` option allows the operation to continue with no key=value pairs. This allows
writing to keys that do not need or expect data. This is aliased as "-f". 
The default is false.

Once the root credential was rotated, only the Vault knows the new root password. This is the same for all root database credentials given to Vault. Therefore, you should create a separate superuser dedicated to the Vault usage which is not used for other purposes.

for more info: [Dynamic Secrets: Database Secrets Engine](https://learn.hashicorp.com/vault/secrets-management/sm-dynamic-secrets) and 
[Database Root Credential Rotation](https://learn.hashicorp.com/vault/security/db-root-rotation).


## Credentials for db management tasks

The next step is to define dynamic db role in Vault for db management. A role is a logical name that maps to a policy used to generate credentials.

Vault does not know what kind of PostgreSQL users you want to create. So, supply the information in SQL to create desired users:

sql/role-admin.sql:

```
CREATE ROLE "{{name}}" WITH SUPERUSER LOGIN PASSWORD '{{password}}' VALID UNTIL '{{expiration}}';
```

The values within the `{{<value>}}` will be filled in by Vault. Notice that "VALID UNTIL" clause. This tells PostgreSQL to revoke the credentials even if Vault is offline or unable to communicate with it.

```
vault write database/roles/admin db_name=postgresql \
        creation_statements=@sql/role-admin.sql \
        default_ttl=1h max_ttl=2h
```

The above command creates a role named "admin" with default TTL of 1 hour, and max TTL of the credential is set to 2 hours. The `role-admin.sql` statement is passed as the role creation statement.

for more info: [Databases Secret Engines](https://www.vaultproject.io/docs/secrets/databases), [PostgreSQL Database Plugin HTTP API](https://www.vaultproject.io/api/secret/databases/postgresql) and [Database Secrets Engine (API)](https://www.vaultproject.io/api-docs/secret/databases).

Now, get a new set of database credentials.

```
vault read database/creds/admin
```

Make sure that you can connect to the "postgres" database using the Vault generated credentials.

to change `max_ttl` after role creation use:

```
vault write database/roles/admin max_ttl=1h
```

### Credentials for applications

Create static db role in Vault for gitea application:

sql/rotation.sql:

```
ALTER USER "{{name}}" WITH PASSWORD '{{password}}';
```

Execute the following command to create a static role, "gitea" with 7-day rotation period.

```
vault write database/static-roles/app-gitea \
        db_name=postgresql \
        rotation_statements=@sql/rotation.sql \
        username="gitea" \
        rotation_period=168h
```

Request PostgreSQL credentials for the "gitea" static role:

```
vault read database/static-creds/app-gitea
```

The password for the static role gets automatically rotated after a configured rotation period. However, there may be a situation requiring you to rotate the password immediately.

```
vault write -f database/rotate-role/app-gitea
```

to change `rotation_period` after role creation use:

```
vault write database/static-roles/app-gitea rotation_period=1h
```


for more info: [Database Static Roles and Credential Rotation](https://learn.hashicorp.com/vault/secrets-management/db-creds-rotation).


Finally you can setup static role and credential rotation for root user.

Or simply disable login as root user:

```
ALTER ROLE root WITH NOLOGIN;
```

## Deploy gitea service

Append following rule to vault `app-policy` to allow Nomad retrieve db secrets:

```
path "database/static-creds/app-*" {
  capabilities = ["read"]
}
```

Update `app-policy`:

```
vault policy write app-policy policy/vault/app-policy.hcl
```

Submit gitea job:

```
nomad run jobs/gitea.nomad
```
